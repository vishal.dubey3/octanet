# Stage 1: Build the Vue.js frontend
FROM node:14 AS frontend-builder

# Set the working directory for the Vue.js app
WORKDIR /app/frontend

# Copy the package.json and package-lock.json files
COPY frontend/package*.json ./

# Install Vue.js dependencies
RUN npm install

# Copy the rest of the Vue.js app files
COPY frontend/ ./

# Build the Vue.js app
RUN npm run build

# Stage 2: Build the Django backend
FROM python:3.8-slim AS django-builder

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory for the Django app
WORKDIR /app/backend

# Copy the requirements.txt file
COPY backend/requirements.txt ./

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the Django app files
COPY backend/ ./

# Stage 3: Final image
FROM python:3.8-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory for the combined app
WORKDIR /app

# Copy the built Vue.js frontend from the previous stage
COPY --from=frontend-builder /app/frontend/dist/ /app/frontend/dist/

# Copy the built Django backend from the previous stage
COPY --from=django-builder /app/backend/ /app/backend/

# Expose port 8000 for the Django backend (adjust as needed)
EXPOSE 8000

# Install gunicorn for serving Django
RUN pip install gunicorn

# Collect Django static files
RUN python backend/manage.py collectstatic --noinput

# Start the Django application using gunicorn
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "backend.wsgi:application"]
