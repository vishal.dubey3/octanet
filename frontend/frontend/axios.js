// axios.js
import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/', // Correct API base URL
  timeout: 5000, // Adjust as needed,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('access_token')}`, // Include the access token
  },
});

export default axiosInstance;