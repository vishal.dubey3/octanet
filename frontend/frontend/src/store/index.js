import { createStore } from 'vuex';

export default createStore({
  state() {
    return {
      accessToken: localStorage.getItem('access_token') || null,
      user: null,
    };
  },
  mutations: {
    setAccessToken(state, token) {
      state.accessToken = token;
    },
    setUser(state, user) {
      state.user = user;
    },
    // ... other mutations
  },
  actions: {
    // ... other actions

    // Add a login action to set the token and user data when the user logs in
    login({ commit }, { accessToken, user }) {
      commit('setAccessToken', accessToken);
      commit('setUser', user);
      localStorage.setItem('access_token', accessToken);
    },

    // Add a logout action to clear the token and user data when the user logs out
    logout({ commit }) {
      commit('setAccessToken', null);
      commit('setUser', null);
      localStorage.removeItem('access_token');
    },
  },
  getters: {
    // ... other getters
    loggedIn: (state) => !!state.accessToken && !!state.user,
    loggedInUser: (state) => state.user,
  },
});
