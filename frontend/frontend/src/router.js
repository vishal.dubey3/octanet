import { createRouter, createWebHistory } from 'vue-router';
//import store from '@/store';
import ThePost from '@/components/blog/ThePost';
import TheAuthor from '@/components/user/TheAuthor';
import PostsByTag from '@/components/blog/PostsByTag';
import AllPosts from '@/components/blog/AllPosts';
import RegistraterUser from '@/components/auth/RegisterUser';
import LoginUser from '@/components/auth/LoginUser';
import LogoutUser from '@/components/auth/LogoutUser';
import LandingPage from '@/components/layout/LandingPage';
import UserProfile from '@/components/user/UserProfile';
import HomePage from '@/components/HomePage';
import ThreeColumnLayout from "@/components/layout/ThreeColumnLayout";

const routes = [
  { path: '/author/:username', component: TheAuthor },
  { path: '/post/:slug', component: ThePost },
  { path: '/tag/:tag', component: PostsByTag },
  { path: '/', component: AllPosts },
  { path: '/landingpage', component: LandingPage, meta: { requiresAuth: true } },
  { path: '/profile', component: UserProfile, meta: { requiresAuth: true } },
  { path: '/register', component: RegistraterUser },
  { path: '/login', component: LoginUser },
  { path: '/logout', component: LogoutUser },
  { path: '/home', component: HomePage },
  {
    path: "/add-post",
    name: "AddPost",
    component: () => import("@/components/blog/AddPost.vue"),
  },
  {
    path: "/feed",
    component: ThreeColumnLayout, // Use the ThreeColumnLayout component
    props: {
      // Pass the allPosts prop to the layout component
      allPosts: [], // Initialize with an empty array or fetch posts here
    },
    children: [
      {
        path: "",
        component: () => import("@/components/blog/AllPosts.vue"), // Your feed component (e.g., AllPosts.vue)
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Navigation guard to protect routes that require authentication


export default router;
