import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client/core';

const httpLink = createHttpLink({
  uri: 'http://localhost:8000/graphql',
});

const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: httpLink,
});

export default apolloClient;