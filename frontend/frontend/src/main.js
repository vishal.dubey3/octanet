import  apolloClient  from './apollo';
import { createApp } from 'vue';
import App from './App.vue';
import { DefaultApolloClient } from '@vue/apollo-composable';
import router from './router';
import { createApolloProvider } from '@vue/apollo-option';
import 'bootstrap/dist/css/bootstrap.css';
//import store from './store'; // Ensure this path is correct
import store from './store'; 



const apolloProvider = createApolloProvider({
  defaultClient: apolloClient,
});

const app = createApp(App);
app.provide(DefaultApolloClient, apolloClient);
app.use(apolloProvider);
app.use(store); 
//app.use(store); // Use the Vuex store here
app.use(router);
app.mount('#app');