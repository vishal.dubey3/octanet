from django.contrib.auth import get_user_model
from graphene import Boolean, ObjectType, List, Field, String
from graphene_django import DjangoObjectType
import graphene
from .models import Profile, Post, Tag, Follow, Like, Comment
from graphene import List, ObjectType, String, Int
from graphene import InputObjectType, Mutation, String, List


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()

class ProfileType(DjangoObjectType):
    class Meta:
        model = Profile

    # Add custom fields for followerCount and followingCount
    followerCount = Int()
    followingCount = Int()

    def resolve_followerCount(self, info):
        return self.follower_count()

    def resolve_followingCount(self, info):
        return self.following_count()

class CommentType(DjangoObjectType):
    class Meta:
        model = Comment

class PostType(DjangoObjectType):
    liked_by_users = List(ProfileType)
    comments = List(CommentType)
    likes_count = Int()

    class Meta:
        model = Post

    def resolve_liked_by_users(self, info):
        return self.liked_by_users.select_related("profile")

    def resolve_comments(self, info):
        return self.comments.all()
    
    def resolve_likes_count(self, info):
        return self.liked_by_users.count()

    # Add resolver for nested user fields in likedByUsers and comments
    def resolve_likedByUsers_user(self, info):
        return [user.user for user in self.liked_by_users.all()]

    def resolve_comments_user(self, info):
        return [comment.user.user for comment in self.comments.all()]
    
class TagType(DjangoObjectType):
    class Meta:
        model = Tag

class FollowType(DjangoObjectType):
    class Meta:
        model = Follow

class LikeType(DjangoObjectType):
    class Meta:
        model = Like



class Query(ObjectType):
    all_posts = List(PostType)
    author_by_username = Field(ProfileType, username=String())
    post_by_slug = Field(PostType, slug=String())
    posts_by_author = List(PostType, username=String())
    posts_by_tag = List(PostType, tag=String())
    user_followers = List(ProfileType, username=String())
    user_following = List(ProfileType, username=String())

    def resolve_all_posts(root, info):
        return (
            Post.objects
            .prefetch_related("tags", "liked_by_users", "comments")
            .select_related("author")
            .all()
        )

    def resolve_author_by_username(root, info, username):
        return Profile.objects.select_related("user").get(user__username=username)

    def resolve_post_by_slug(root, info, slug):
        post = Post.objects.prefetch_related(
            "tags", "liked_by_users__user", "comments__user__user"
        ).select_related("author").get(slug=slug)
        return post

    def resolve_posts_by_author(root, info, username):
        return (
            Post.objects.prefetch_related("tags")
            .select_related("author")
            .filter(author__user__username=username)
        )

    def resolve_posts_by_tag(root, info, tag):
        return (
            Post.objects.prefetch_related("tags")
            .select_related("author")
            .filter(tags__name__iexact=tag)
        )

    def resolve_user_followers(root, info, username):
        return (
            Profile.objects.filter(user__followers__follower__user__username=username)
            .distinct()
            .select_related("user")
        )

    def resolve_user_following(root, info, username):
        return (
            Profile.objects.filter(user__following__following__user__username=username)
            .distinct()
            .select_related("user")
        )
    
class LikePostMutation(Mutation):
    class Arguments:
        post_id = Int(required=True)
        user_id = Int(required=True)

    success = Boolean()

    def mutate(self, info, post_id, user_id):
        try:
            post = Post.objects.get(id=post_id)
            user = get_user_model().objects.get(id=user_id)
            
            # Check if the user has already liked the post
            existing_like = Like.objects.filter(post=post, user=user).first()
            if existing_like:
                # Unlike the post
                existing_like.delete()
                success = True
            else:
                # Like the post
                Like.objects.create(post=post, user=user)
                success = True
        except Exception:
            success = False

        return LikePostMutation(success=success)   



class AddCommentMutation(Mutation):
    class Arguments:
        post_id = Int(required=True)
        text = String(required=True)

    comment = Field(CommentType)

    def mutate(self, info, post_id, text):
        user = info.context.user  # Assuming you have authentication
        post = Post.objects.get(id=post_id)
        comment = Comment(user=user, post=post, text=text)
        comment.save()
        return AddCommentMutation(comment=comment)
    
class FollowUserMutation(Mutation):
    class Arguments:
        user_id_to_follow = Int(required=True)

    success = Boolean()

    def mutate(self, info, user_id_to_follow):
        user = info.context.user
        try:
            user_to_follow = get_user_model().objects.get(id=user_id_to_follow)
            if user != user_to_follow:  # Ensure users can't follow themselves
                # Check if the user is already following the target user
                if not Follow.objects.filter(follower=user, following=user_to_follow).exists():
                    Follow.objects.create(follower=user, following=user_to_follow)
                success = True
            else:
                success = False  # Cannot follow oneself
        except Exception:
            success = False

        return FollowUserMutation(success=success)
    
class UnfollowUserMutation(Mutation):
    class Arguments:
        user_id_to_unfollow = Int(required=True)

    success = Boolean()

    def mutate(self, info, user_id_to_unfollow):
        user = info.context.user
        try:
            user_to_unfollow = get_user_model().objects.get(id=user_id_to_unfollow)
            if user != user_to_unfollow:
                follow = Follow.objects.filter(follower=user, following=user_to_unfollow).first()
                if follow:
                    follow.delete()
                success = True
            else:
                success = False  # Cannot unfollow oneself
        except Exception:
            success = False

        return UnfollowUserMutation(success=success)

class Mutation(ObjectType):
    like_post = LikePostMutation.Field()
    add_comment = AddCommentMutation.Field()
    follow_user = FollowUserMutation.Field()  
    unfollow_user = UnfollowUserMutation.Field()


schema = graphene.Schema(query=Query)
