from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import Profile, Post, Tag, Follow, Like, Comment

User = get_user_model()

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    model = Profile

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    model = Tag

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    model = Post

    list_display = (
        'id',
        'title',
        'subtitle',
        'slug',
        'publish_date',
        'published',
        'author',
    )

    list_filter = (
        'published',
        'publish_date',
        'author',
    )

    list_editable = (
        'title',
        'subtitle',
        'slug',
        'publish_date',
        'published',
        'author',
    )

    search_fields = (
        'title',
        'subtitle',
        'slug',
        'body',
        'author__user__username',  # Search by username of the author
    )

    prepopulated_fields = {
        'slug': (
            'title',
            'subtitle',
        )
    }

    date_hierarchy = 'publish_date'
    save_on_top = True

@admin.register(Follow)
class FollowAdmin(admin.ModelAdmin):
    model = Follow

@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    model = Like

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    model = Comment