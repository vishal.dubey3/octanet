from django.contrib import admin
from django.urls import path, include  # Import include to include app-level URLs
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView



urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),  # JWT token obtain endpoint
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),  # JWT token refresh endpoint
    path('api/', include('users.urls')),  # Include the app-level URLs for user management
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),
]
